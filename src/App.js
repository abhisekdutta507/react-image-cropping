import React, { useState } from 'react';
import ReactCrop from 'react-image-crop';
import yamaha from './assets/images/yamaha.jpg';
import 'react-image-crop/dist/ReactCrop.css';
import './App.css';

function App() {
  const [state, setState] = useState({
    crop: { aspect: 16 / 9 },

    image: undefined,

    croppedImg: undefined
  });

  /**
   * @param {HTMLImageElement} image - Image File Object
   * @param {Object} crop - crop Object
   * @param {String} fileName - Name of the returned file in Promise
   */
  const getCroppedImg = (image, crop, fileName) => {
    const canvas = document.createElement('canvas');
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext('2d');
  
    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height,
    );
  
    // As Base64 string
    return new Promise(next => {
      next(canvas.toDataURL('image/jpeg'));
    });

    // As a blob
    // return new Promise(next => {
    //   canvas.toBlob(blob => {
    //     if (!blob) blob = undefined;
    //     else blob.name = fileName;
    //     next(blob);
    //   }, 'image/jpeg', 1);
    // });
  }

  const cropComplete = async (crop) => {
    const croppedImg = await getCroppedImg(state.image, crop, 'yamaha').catch();
    setState({ ...state, croppedImg });
  }

  const imageLoaded = (image) => {
    setState({ ...state, image });
    return false; // Return false when setting crop state in here.
  };


  return (
    <div className="App">
      <header className="App-header">
        
      </header>
      <div className="App-body">
        <ReactCrop
          src={yamaha}
          crop={state.crop}
          onImageLoaded={imageLoaded}
          onChange={crop => setState({ ...state, crop })}
          onComplete={cropComplete}
        />;
      </div>
      <footer>

      </footer>
    </div>
  );
}

export default App;
